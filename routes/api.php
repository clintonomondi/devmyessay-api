<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'App\Http\Controllers\AuthController@login');
    Route::post('getOtp', 'App\Http\Controllers\AuthController@getOtp');
    Route::post('verify', 'App\Http\Controllers\AuthController@verify');
    Route::post('resetpassword', 'App\Http\Controllers\AuthController@resetpassword');
    Route::post('signup', 'App\Http\Controllers\AuthController@signup');
    Route::get('/client/order', 'App\Http\Controllers\PagesController@index')->name('student.order');
    Route::post('/client/loadAcademics', 'App\Http\Controllers\ApiController@loadAcademics')->name('student.loadAcademics');
    Route::get('/client/getIndexdata', 'App\Http\Controllers\ApiController@getIndexdata')->name('student.getIndexdata');

    Route::post('/client/postorder', 'App\Http\Controllers\PagesController@postorder')->name('student.postorder');
    Route::post('subscribe', 'App\Http\Controllers\ApiController@subscribe');
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'App\Http\Controllers\AuthController@logout');
        Route::get('user', 'App\Http\Controllers\AuthController@user');
        Route::get('getAccessToken', 'App\Http\Controllers\ApiController@getAccessToken');
        Route::post('updateInfo', 'App\Http\Controllers\AuthController@updateInfo');
        Route::post('updatePassword', 'App\Http\Controllers\AuthController@updatePassword');


        Route::post('/client/postorderAuth', 'App\Http\Controllers\PagesController@postorderAuth')->name('student.postorderAuth');
        Route::get('/client/confirm/{id}', 'App\Http\Controllers\PagesController@confirm')->name('student.confirm');
        Route::get('/client/getOrderData', 'App\Http\Controllers\ApiController@getOrderData')->name('student.getOrderData');
        Route::post('/client/orderStatus', 'App\Http\Controllers\PagesController@orderStatus')->name('student.orderStatus');

        Route::get('/client/getDefaultOrderService', 'App\Http\Controllers\ApiController@getDefaultOrderService')->name('student.getDefaultOrderService');


        Route::get('/client/orders/view/{id}', 'App\Http\Controllers\OrdersController@vieworder')->name('student.vieworder');
        Route::get('/client/orders', 'App\Http\Controllers\OrdersController@orders')->name('student.orders');
        Route::get('/client/inprogress', 'App\Http\Controllers\OrdersController@inprogress')->name('student.inprogress');
        Route::get('/client/onhold', 'App\Http\Controllers\OrdersController@onhold')->name('student.onhold');
        Route::get('/client/revised', 'App\Http\Controllers\OrdersController@revised')->name('student.revised');
        Route::get('/client/completed', 'App\Http\Controllers\OrdersController@completed')->name('student.completed');
        Route::get('/client/checkout/{id}', 'App\Http\Controllers\OrdersController@checkout')->name('student.checkout');
        Route::get('/client/order/add/{id}', 'App\Http\Controllers\OrdersController@add')->name('student.add_order');
        Route::post('/client/order/add_post/{id}', 'App\Http\Controllers\OrdersController@post_add_payment')->name('student.post_add_payment');
        Route::post('/client/order/edit_post/{id}', 'App\Http\Controllers\OrdersController@post_edit_payment');
        Route::get('/client/order/adjust/{id}', 'App\Http\Controllers\OrdersController@adjust_order')->name('student.adjust_order');
        Route::get('/client/order/delete/{id}', 'App\Http\Controllers\OrdersController@delete_order')->name('student.delete_order');
        Route::get('/client/order/edit/{id}', 'App\Http\Controllers\OrdersController@edit_order');
        Route::post('/client/order/update/{id}', 'App\Http\Controllers\OrdersController@update');
        Route::post('/client/order/adjust/post/{id}', 'App\Http\Controllers\PagesController@post_adjust_order')->name('student.post_adjust_order');
        Route::post('/client/order/addfile/{id}', 'App\Http\Controllers\PagesController@addfile')->name('student.addfile');
        Route::post('/client/order/sendRevised/{id}', 'App\Http\Controllers\OrdersController@sendRevised')->name('student.sendRevised');
        Route::post('updateAdsStatus', 'App\Http\Controllers\OrdersController@updateAdsStatus');

        Route::get('/profile', 'App\Http\Controllers\HomeController@profile')->name('profile');
        Route::post('/profile/update', 'App\Http\Controllers\HomeController@studentupdate')->name('student.update_profile');
        Route::post('/password/update', 'App\Http\Controllers\HomeController@updatepassword')->name('student.update_password');


        Route::post('/client/fund/request/{id}', 'App\Http\Controllers\FundController@requestRefund')->name('fund.requestRefund');
        Route::get('/client/fund/refunds', 'App\Http\Controllers\FundController@refunds')->name('fund.refunds');



        Route::get('messages/{id}', 'App\Http\Controllers\ChatsController@fetchMessages');
        Route::post('messages/{id}', 'App\Http\Controllers\ChatsController@sendMessage');
        Route::get('notifications', 'App\Http\Controllers\ChatsController@notifications');
        Route::get('updateNotif/{id}', 'App\Http\Controllers\ChatsController@updateNotif');
        Route::post('refer', 'App\Http\Controllers\ApiController@refer');
        Route::get('referals', 'App\Http\Controllers\ApiController@referals');



        //Writer Controllers

        Route::get('/writer/getOrderData', 'App\Http\Controllers\WriterController@getOrderData');
        Route::get('/writer/orders', 'App\Http\Controllers\WriterController@orders');
        Route::get('/order/bid/{id}', 'App\Http\Controllers\WriterController@bid');
        Route::post('/writer/upload/{id}', 'App\Http\Controllers\WriterController@upload');

        Route::get('/writer/inprogress', 'App\Http\Controllers\WriterController@inprogress');
        Route::get('/writer/onhold', 'App\Http\Controllers\WriterController@onhold');
        Route::get('/writer/revised', 'App\Http\Controllers\WriterController@revised');
        Route::get('/writer/completed', 'App\Http\Controllers\WriterController@completed');
    });
});
