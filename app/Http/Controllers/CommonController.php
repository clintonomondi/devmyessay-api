<?php

namespace App\Http\Controllers;

use App\Models\Agency;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    public static  function getDeadline($id,$date){
        $time='';
        $current =new Carbon($date);
        $ugency=Agency::find($id);
        if($ugency->type=='hour'){
            $trialExpires = $current->addHours($ugency->value);
            return $trialExpires->format('Y-m-d H:i:s');
        }
         if($ugency->type=='day'){
            $trialExpires = $current->addDays($ugency->value);
             return $trialExpires->format('Y-m-d H:i:s');
        }
         if($ugency->type=='week'){
            $trialExpires = $current->addWeeks($ugency->value);
             return $trialExpires->format('Y-m-d H:i:s');
        }
         if($ugency->type=='month'){
            $trialExpires = $current->addMonths($ugency->value);
             return $trialExpires->format('Y-m-d H:i:s');
        }
        return $time;

    }


}
