<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Agency;
use App\Models\Instruction;
use App\Models\Order;
use App\Models\Ordervalue;
use App\Models\Other_services;
use App\Models\PagesPowerpoint;
use App\Models\PaymentLogs;
use App\Models\PayPalLogs;
use App\Models\Revision;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class OrdersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public  function sendRevised(Request $request,$id){
        $check=Revision::where('order_id',$id)->where('status','pending')->count();
        if($check>0){
            return ['status'=>false,'message'=>'A request for revision for this order is still pending'];
        }
        $order=Order::find($id);
        $request['status']='revised';
        $order->update($request->all());
        $request['status']='pending';
        $request['order_id']=$id;
        $re=Revision::create($request->all());

        $Notif_Api=env('Notif_Api');
        $admins=Admin::where('role','admin')->get();
        foreach ($admins as $admin){
            $data2=['message'=>'A client with email '.Auth::user()->email.' has requested for revision for Order ID  '.$order->code.'. @devmyessay','phone'=>$admin->phone];
            $response2 = Http::withHeaders(['Content-Type'=>'application/json'])->post($Notif_Api.'sms',$data2);
        }
        return ['status'=>true,'message'=>'Request sent successfully'];
    }

    public  function post_add_payment(Request $request,$id){
        $order=Order::find($request->id);
        $request['trans_id']=substr(md5(microtime()), 0, 10);
        $request['order_id']=$id;
        $request['amount_paid'] = $request->amount_to_pay;
        $request['reason'] = 'Addition of payment';
        $request['amount'] = $order->amount + $request->amount_to_pay;
        $order->update($request->all());
        $log = PaymentLogs::create($request->all());

        $paypal_logs=PayPalLogs::create($request->all());
        return ['status'=>true,'message'=>'Payment was successfully made.Redirecting...'];
    }

    public  function post_edit_payment(Request $request,$id){
        $order=Order::find($request->id);
        $request['trans_id']=substr(md5(microtime()), 0, 10);
        $request['order_id']=$id;
        $request['amount_paid'] = $request->amount_to_pay;
        $request['reason'] = 'Editing of payment';
        $request['amount'] = $order->amount + $request->amount_to_pay;

        $log = PaymentLogs::create($request->all());
        $paypal_logs=PayPalLogs::create($request->all());
        return ['status'=>true,'message'=>'Payment was successfully made.Redirecting...'];
    }

    public  function orders(){
        $orders=\App\Models\Order::orderBy('id','desc')->where('user_id',Auth::user()->id)->where('status','pending')->get();
        return ['orders'=>$orders];
    }

    public  function inprogress(){
        $orders=\App\Models\Order::orderBy('id','desc')->where('user_id',Auth::user()->id)->where('status','inprogress')->get();
        return ['orders'=>$orders];
    }
    public  function onhold(){
        $orders=\App\Models\Order::orderBy('id','desc')->where('user_id',Auth::user()->id)->where('status','onhold')->get();
        return ['orders'=>$orders];
    }
    public  function revised(){
        $orders=\App\Models\Order::orderBy('id','desc')->where('user_id',Auth::user()->id)->where('status','revised')->get();
        return ['orders'=>$orders];
    }
    public  function completed(){
        $orders=\App\Models\Order::orderBy('id','desc')->where('user_id',Auth::user()->id)->where('status','completed')->get();
        return ['orders'=>$orders];
    }


    public  function checkout($id)
    {
        $paypal_access_token= (new ApiController)->getAccessToken();
        $paypal_client_token_url=env('PAYPAL_CLIENT_TOKEN_URL');
        $client_id=env('CLIENT_ID');
        $order = Order::find($id);
        $wallet = Wallet::where('user_id', Auth::user()->id)->sum('amount');
        $amount = $order->amount  - $wallet;
        if ($amount < 0) {
            $amount_to_pay = 0;
            $wallet = $amount * -1;
        } else {
            $amount_to_pay = $amount;
            $wallet = 0;
        }
        return ['order' => $order, 'id' => $id, 'amount_to_pay' => $amount_to_pay, 'wallet' => $wallet,'paypal_client_token_url'=>$paypal_client_token_url,'paypal_access_token'=>$paypal_access_token,'client_id'=>$client_id];
    }

    public  function vieworder($id){
        $order=DB::select( DB::raw("SELECT *,
(SELECT NAME FROM `subjects` B WHERE B.id=A.subject_id)subject,
(SELECT VALUE FROM `agencies` B WHERE B.id=A.agency_id)agency,
(SELECT prefix FROM `agencies` B WHERE B.id=A.agency_id)prefix,
(SELECT NAME FROM `academics` B WHERE B.id=A.academic_id)academic,
(SELECT NAME FROM `typeofpapers` B WHERE B.id=A.typeofpaper_id)paper,
(SELECT writer_id FROM `users` B WHERE B.id=A.writer_id)writer_id,
(SELECT NAME FROM `typeofservices` B WHERE B.id=A.typeofservice_id)service
 FROM `orders` A WHERE id='$id'") );
        $writer_app=env('Writer_Application');
        $files=DB::select( DB::raw("SELECT id,order_id,name,type,DATE(created_at) AS date FROM `files` WHERE order_id='$id'") );
        $payments=Order::find($id)->payment_logs;
        $other_services=DB::select( DB::raw("SELECT *,
(SELECT NAME FROM `additonals` B WHERE B.id=A.additional_id)other_service_name
 FROM `other_services` A WHERE order_id='$id'") );
        $instructions=Instruction::where('order_id',$id)->get();
        return ['instructions'=>$instructions,'orders'=>$order,'files'=>$files,'payments'=>$payments,'writer_app'=>$writer_app,'otherservices'=>$other_services];
    }

public  function adjust_order($id){
    $order=Order::find($id);
    $services=\App\Models\Typeofservice::all();
    $papers=\App\Models\Typeofpaper::all();
    $subjects=\App\Models\Subject::all();
    $agencys=\App\Models\Agency::all();
    $academics=\App\Models\Academic::all();
    $additionals=\App\Models\Additonal::all();
    $ordervalue=Ordervalue::where('order_id',$id)->first();
    return view('student.adjust_order',compact('services','papers','subjects','agencys','academics','additionals','order','ordervalue'));

}

public  function edit_order($id){
        $order=Order::find($id);
        $agencys=\App\Models\Agency::all();
    $powerpoint=PagesPowerpoint::where('name','powerpoint')->sum('amount');
    $paypal_access_token= (new ApiController)->getAccessToken();
    $paypal_client_token_url=env('PAYPAL_CLIENT_TOKEN_URL');
    $client_id=env('CLIENT_ID');

    return ['order'=>$order,'agencys'=>$agencys,'powerpoint'=>$powerpoint,
        'paypal_access_token'=>$paypal_access_token,'paypal_client_token_url'=>$paypal_client_token_url,'client_id'=>$client_id];

}

public  function delete_order($id){
        $order=Order::find($id);
        $order->delete();
        $service=Other_services::where('order_id',$id)->delete();
        $value=Ordervalue::where('order_id',$id)->delete();
    return ['status'=>true,'message'=>'Order removed successfully'];
}

public  function update(Request $request,$id){
        $order=Order::find($id);
            $request['amount'] = $request->total;
        $order->update($request->all());
        $request['order_id']=$id;
        if(!empty($request->newinstruction)){
            $request['instruction']=$request->newinstruction;
            $instructions=Instruction::create($request->all());
        }
        $writer=User::find($order->writer_id);

    $Notif_Api=env('Notif_Api');
    $data=['message'=>'Client for Order ID  '.$order->code.' has updated the order', 'email'=>$writer->email, 'subject'=>'ORDER UPDATED'];
    $data2=['message'=>'Client for Order ID  '.$order->code.' has updated the order. @devmyessay','phone'=>$writer->phone];
    $response = Http::withHeaders(['Content-Type'=>'application/json'])->post($Notif_Api.'email',$data);
    $response2 = Http::withHeaders(['Content-Type'=>'application/json'])->post($Notif_Api.'sms',$data2);


    return ['status'=>true,'message'=>'Order updated successfully'];
}


    public  function updateAdsStatus(Request $request){
        $ads=Order::find($request->id);
        $ads->update($request->all());
        return ['status'=>true,'message'=>'Order updated successfully'];
    }



}
