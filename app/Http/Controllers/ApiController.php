<?php

namespace App\Http\Controllers;

use App\Models\Academic;
use App\Models\Admin;
use App\Models\Agency;
use App\Models\Features;
use App\Models\Order;
use App\Models\PagesPowerpoint;
use App\Models\Refer;
use App\Models\ReferalCode;
use App\Models\Subscriptions;
use App\Models\Typeofpaper;
use App\Models\Typeofservice;
use App\Models\User;
use App\Models\Wallet;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;

class ApiController extends Controller
{


    public  function loadAcademics(Request $request){
        $word="Associate's";
        $paper=Typeofpaper::all();
        $paper_id=Typeofpaper::select('id')->where('name','Dissertation Paper')->first();
        if(Str::contains($request->name, 'Dissertation')){
            $academic=Academic::where('dis','Yes')->get();
            $agency=Agency::where('dis','Yes')->get();
            $dis='yes';
            $agency_id=Agency::select('id')->where('dis','Yes')->where('value','2')->where('type','month')->first();
            $academic_id=Academic::select('id')->where('dis','Yes')->where('name',$word)->first();
        }else{
            $academic=Academic::where('dis','No')->get();
            $agency=Agency::where('dis','No')->get();
            $dis='no';
            $academic_id='1';
            $agency_id=Agency::select('id')->where('dis','No')->where('value','15')->where('type','day')->first();
        }


        if(Str::contains($request->name, 'Question')){
            $qustion='yes';
        }else{
            $qustion='no';
        }

        return ['academic'=>$academic,'agency'=>$agency,'question'=>$qustion,'dis'=>$dis,'agency_id'=>$agency_id,'academic_id'=>$academic_id,'paper'=>$paper,'paper_id'=>$paper_id];

    }

    public  function getIndexdata(){
        $discount=PagesPowerpoint::where('name','discount')->sum('amount');
        $refer=PagesPowerpoint::where('name','refer')->sum('amount');
        $free=Features::where('type','free')->get();
        $charged=Features::where('type','charged')->get();
        return ['discount'=>$discount,'refer'=>$refer,'free'=>$free,'charged'=>$charged];
    }

    public  function getDefaultOrderService(){
        $services=Typeofservice::all();
        $paper=Typeofpaper::all();

        return ['status'=>true,'services'=>$services,'paper'=>$paper];
    }



    public  function getOrderData(){
        $pending=Order::where('status','pending')->where('user_id',Auth::user()->id)->count();
        $inprogress=Order::where('status','inprogress')->where('user_id',Auth::user()->id)->count();
        $revised=Order::where('status','revised')->where('user_id',Auth::user()->id)->count();
        $completed=Order::where('status','completed')->where('user_id',Auth::user()->id)->count();
        $onhold=Order::where('status','onhold')->where('user_id',Auth::user()->id)->count();
        $discount=PagesPowerpoint::where('name','discount')->sum('amount');
        $wallet=Wallet::where('user_id',Auth::user()->id)->sum('amount');

        return ['status'=>true,'pending'=>$pending,'inprogress'=>$inprogress,
            'revised'=>$revised,'completed'=>$completed,'discount'=>$discount,'onhold'=>$onhold,'wallet'=>$wallet];
    }

    public  function getAccessToken(){
        $value = Cache::remember('paypal_access_token', '28000', function () {
            $user_name=env('PAYPAL_USERNAME');
            $password=env('PAYPAL_PASSWORD');
            $url=env('PAYPAL_ACCESS_TOKEN_URL');
            $client = new Client();
            $response = $client->request('POST', $url,
                [
                    'auth' => [$user_name, $password],
                    'headers' => [
                        'Content-Type'=>'application/x-www-form-urlencoded',
                    ],
                    'form_params' => [
                        'grant_type'=>'client_credentials',
                    ],
                ]
            );
            $data=json_decode($response->getBody()->getContents());
            return $data->access_token;
        });
       return  'Bearer    '.$value;
    }

    public  function subscribe(Request $request){
        $user=Subscriptions::where('email',$request->email)->count();
        if($user>0){
            return ['status'=>false,'message'=>'Email already exist'];
        }
        $data=Subscriptions::create($request->all());

        $admins=Admin::where('role','admin')->get();
        $Notif_Api=env('Notif_Api');
        foreach ($admins as $admin){
            $data2=['message'=>'A client with email:  '.$request->email.' has sent a subscription. @devmyessay','phone'=>$admin->phone];
            $response2 = Http::withHeaders(['Content-Type'=>'application/json'])->post($Notif_Api.'sms',$data2);
        }

        return ['status'=>true,'message'=>'Subscription done successfully'];
    }



    public  function refer(Request $request){
        $validated = $request->validate([
            'refer_email' => 'required|email',
        ]);
        $check=User::where('email',$request->refer_email)->count();
        $check2=Refer::where('refer_email',$request->refer_email)->count();
        if($check>0){
            return ['status'=>false,'message'=>'Email is already in the system'];
        }
        if($check2>0){
            return ['status'=>false,'message'=>'Email has already been invited, pleas try another email'];
        }
        $code=mt_rand(100000,999999);
        $request['code']='D'.$code;
        $request['user_id']=Auth::user()->id;
        $data=Refer::create($request->all());

        $Notif_Api=env('Notif_Api');
        $data=['message'=>'You have been invited to https://devmyessay.com  by  '.Auth::user()->name.'. Please use Promo code ='.$request->code.' to place an order', 'email'=>$request->refer_email, 'subject'=>'DEVMYESSAY INVITATION'];
        $response = Http::withHeaders(['Content-Type'=>'application/json'])->post($Notif_Api.'email',$data);

        $admins=Admin::where('role','admin')->get();
        foreach ($admins as $admin){
            $data2=['message'=>'A client with name '.Auth::user()->name.' has   referred  '.$request->refer_email.'. @devmyessay','phone'=>$admin->phone];
            $response2 = Http::withHeaders(['Content-Type'=>'application/json'])->post($Notif_Api.'sms',$data2);
        }

        return ['status'=>true,'message'=>'You have successfully invited '.$request->refer_email.'.Please ask them to use your promo code sent to them when placing an order'];

    }

    public  function referals(Request $request){
        $user_id=Auth::user()->id;
        $data = DB::select( DB::raw("SELECT *,
(SELECT email FROM users B WHERE B.id=A.user_id)email,
(SELECT name FROM users B WHERE B.id=A.user_id)name,
(SELECT code FROM orders B WHERE B.id=A.order_id)order_code
 FROM `refering_logs` A WHERE referer_id='$user_id'") );

        $check=ReferalCode::where('user_id',Auth::user()->id)->count();
        if($check<=0){
            $request['code']='DE'.mt_rand(100000,999999);
            $request['user_id']=Auth::user()->id;
            $da=ReferalCode::create($request->all());
        }
        $codes=ReferalCode::where('user_id',Auth::user()->id)->first();

        return ['status'=>true,'referals'=>$data,'codes'=>$codes];
    }
}
