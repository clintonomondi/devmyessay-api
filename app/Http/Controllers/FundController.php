<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Refund;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class FundController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public  function requestRefund(Request $request,$id){
        $validatedData = $request->validate([
            'amount_to_pay' => 'required|numeric',
            'reason' => 'required',
        ]);

        $order=Order::find($id);

        $chck=Refund::where('order_id',$id)->where('status','inprogress')->count();
        if($chck>0){
            return ['status'=>false,'message'=>'Similar request is still inprogress'];
        }
        $request['user_id']=Auth::user()->id;
        $request['order_id']=$id;
        $request['status']='onhold';
        $order->update($request->all());

        $request['status']='pending';
        $request['amount']=$request->amount_to_pay;
        $data=Refund::create($request->all());
        return ['status'=>true,'message'=>'Request submitted successfully'];
    }
    public  function refunds(){
        $user_id=Auth::user()->id;
        $funds=DB::select( DB::raw("SELECT *,
(SELECT code FROM orders B WHERE B.id=A.order_id)code
 FROM `refunds` A WHERE user_id='$user_id'") );
         return ['refunds'=>$funds];
    }
}
