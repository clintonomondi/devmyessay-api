<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public  function  profile(){
        $user=User::find(Auth::user()->id);
        return view('profile',compact('user'));
    }

    public  function studentupdate(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);
        $user=User::find(Auth::user()->id);
        $user->update($request->all());
        return redirect()->back()->with('success','Information updated successfully');
    }

    public  function updatepassword(Request $request){
        $this->validate($request, [
            'currentpass' => 'required|min:5',
            'newpass' => 'required|min:5',
            'repass' => 'required',
        ]);
        $currentpass = auth()->user()->password;
        if (!Hash::check($request['currentpass'], $currentpass)) {
            return redirect()->back()->with('errors', 'The current password is wrong');
        } elseif ($request->input('newpass') !== $request->input('repass')) {
            return redirect()->back()->with('errors', 'The new passwords do not match');
        }
        $id = auth()->user()->id;
        $currentUser = User::findOrFail($id);
        $currentUser->password = Hash::make($request->input('newpass'));
        $currentUser->save();

        return redirect()->back()->with('success', 'Password changed successfully');
    }
}
