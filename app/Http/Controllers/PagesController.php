<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Agency;
use App\Models\Files;
use App\Models\Instruction;
use App\Models\Order;
use App\Models\Ordervalue;
use App\Models\Other_services;
use App\Models\PagesPowerpoint;
use App\Models\PaymentLogs;
use App\Models\PayPalLogs;
use App\Models\ReferalCode;
use App\Models\ReferingLogs;
use App\Models\User;
use App\Models\Wallet;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class PagesController extends Controller
{


    public  function submitcontact(Request $request){
        return redirect()->back()->with('success','Information submitted successfully');
    }
    public  function index(){
        $services=\App\Models\Typeofservice::all();
        $papers=\App\Models\Typeofpaper::all();
        $subjects=\App\Models\Subject::all();
        $agencys=\App\Models\Agency::where('dis','No')->get();
        $academics=\App\Models\Academic::where('dis','No')->get();
        $additionals=\App\Models\Additonal::where('incremental','No')->get();
        $additionals2=\App\Models\Additonal::where('incremental','Yes')->get();
        $discount=PagesPowerpoint::where('name','discount')->sum('amount');
        $page=PagesPowerpoint::where('name','page')->sum('amount');
        $powerpoint=PagesPowerpoint::where('name','powerpoint')->sum('amount');
        $agency_id=Agency::select('id')->where('dis','No')->where('value','15')->where('type','day')->first();

        return ['agency_id'=>$agency_id,'powerpoint'=>$powerpoint,'services'=>$services,'paper'=>$papers,'subjects'=>$subjects,'additionals2'=>$additionals2,'agencys'=>$agencys,'academics'=>$academics,'additionals'=>$additionals,'page'=>$page,'discount'=>$discount];
    }




    public  function postorder(Request $request){

        if(empty($request->date)){
            return['status'=>false,'message'=>'We are not able to get todays date'];
        }
        if(empty($request->title)){
            return['status'=>false,'message'=>'Please enter title'];
        }
        if(empty($request->subject_id)){
            return['status'=>false,'message'=>'Please select subject'];
        }
        if(empty($request->amount)){
            return['status'=>false,'message'=>'We cannot identify the total amount'];
        }
            if(empty($request->email)){
                return['status'=>false,'message'=>'Please provide email address'];
            }

            $countEmail=User::where('email',$request->email)->count();
            $credentials = $request->only('email', 'password');
            $request['password']=Hash::make($request->password);
            if ($countEmail > 0) {
                if (Auth::attempt($credentials)) {

                } else {
                    return ['status' => false, 'message' => 'This email  already exist but you entered wrong password'];
                }
            } else {
                $register = User::create($request->all());
                if (Auth::attempt($credentials)) {

                } else {
                    return ['status' => false, 'message' => 'We are not able to log you in, please use login page'];
                }
            }
        $request['deadline']=CommonController::getDeadline($request->agency_id,$request->date);
        $request['submited_at']=$request->date;
        $request['code']=substr(md5(microtime()), 0, 10);
        $request['user_id']=Auth::user()->id;
        $orders=Order::create($request->all());
        $request['order_id']=$orders->id;
        $order_charges=Ordervalue::create($request->all());
        if(!empty($request->additional_id)){
            foreach ($request->additional_id as $additional){
                $request['additional_id']=$additional;
                Other_services::create($request->all());
            }
        }
        $instructions=Instruction::create($request->all());
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        $profile=User::find(Auth::user()->id);
        $wallet=Wallet::where('user_id',Auth::user()->id)->sum('amount');


        $Notif_Api=env('Notif_Api');
        $admins=Admin::where('role','admin')->get();
        foreach ($admins as $admin){
            $data2=['message'=>'A client with email '.Auth::user()->email.' is trying to place an order  '.'. @devmyessay','phone'=>$admin->phone];
            $response2 = Http::withHeaders(['Content-Type'=>'application/json'])->post($Notif_Api.'sms',$data2);
        }

        return ['order_id'=>$orders->id,'status'=>true,'user'=>$profile,'wallet'=>$wallet,'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()

        ];

    }

    public  function postorderAuth(Request $request){

        if(empty($request->date)){
            return['status'=>false,'message'=>'We are not able to get todays date'];
        }
        if(empty($request->amount)){
            return['status'=>false,'message'=>'We cannot identify the total amount'];
        }

        $request['deadline']=CommonController::getDeadline($request->agency_id,$request->date);
        $request['submited_at']=$request->date;
        $request['code']=substr(md5(microtime()), 0, 10);
        $request['user_id']=Auth::user()->id;
        $orders=Order::create($request->all());
        $request['order_id']=$orders->id;
        $order_charges=Ordervalue::create($request->all());
        if(!empty($request->additional_id)){
            foreach ($request->additional_id as $additional){
                $request['additional_id']=$additional;
                Other_services::create($request->all());
            }
        }

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        $profile=User::find(Auth::user()->id);
        $wallet=Wallet::where('user_id',Auth::user()->id)->sum('amount');


        $Notif_Api=env('Notif_Api');
        $admins=Admin::where('role','admin')->get();
        foreach ($admins as $admin){
            $data2=['message'=>'A client with email '.Auth::user()->email.' is trying to place an order  '.'. @devmyessay','phone'=>$admin->phone];
            $response2 = Http::withHeaders(['Content-Type'=>'application/json'])->post($Notif_Api.'sms',$data2);
        }
        return ['order_id'=>$orders->id,'status'=>true,'user'=>$profile,'wallet'=>$wallet,'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()

        ];

    }


public  function addfile(Request $request,$id){
        $request['order_id']=$id;
        $request['user_id']=Auth::user()->id;
    if(!empty($request->files)) {
        if (($request->has('files'))) {
            $files = $request->file('files');
            foreach ($files as $file) {
                $fileNameWithExt = $file->getClientOriginalName();
                $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                $extesion = $file->getClientOriginalExtension();
                $fileNameToStore = $filename . '_' . time() . '.' . $extesion;
                $path = $file->storeAs('/public/avatars', $fileNameToStore);
                $request['name'] = $fileNameToStore;
                $doc = Files::create($request->all());
            }
        }
    }
    return ['status'=>true,'message'=>'Files uploaded successfully'];
}


    public  function confirm($id){
        $order=DB::select( DB::raw("SELECT *,
(SELECT NAME FROM `subjects` B WHERE B.id=A.subject_id)SUBJECT,
(SELECT VALUE FROM `agencies` B WHERE B.id=A.agency_id)agency,
(SELECT prefix FROM `agencies` B WHERE B.id=A.agency_id)prefix,
(SELECT NAME FROM `academics` B WHERE B.id=A.academic_id)academic,
(SELECT NAME FROM `typeofpapers` B WHERE B.id=A.typeofpaper_id)paper,
(SELECT NAME FROM `typeofservices` B WHERE B.id=A.typeofservice_id)service
 FROM `orders` A WHERE id='$id'") );

        return ['order'=>$order];
    }

    public  function checkuser(Request $request){
        $countEmail=User::where('email',$request->email)->count();
        if($countEmail>0){
            return ['status'=>true];
        }else{
            return ['status'=>false];
        }
    }



    public  function post_adjust_order(Request $request,$id){
        $order=Order::find($id);
        $current_amount=PaymentLogs::where('order_id',$id)->where('type','PAYMENT')->sum('amount_paid');

        $amount_diff=$request['amount']-$current_amount;
        if($amount_diff<=0){
            $order->update($request->all());
            $ordervalue=Ordervalue::find($request->oder_value_id);
            $ordervalue->update($request->all());
            return redirect()->back()->with('success','Information saved successfully');
        }else{
        $request['status']='pending';
            $order->update($request->all());
            $ordervalue=Ordervalue::find($request->oder_value_id);
            $ordervalue->update($request->all());


            $amount=$amount_diff;
            $flag='adjusting_payment';
            return view('student.checkout',compact('client_token','amount','id','flag'));

        }

    }

    public  function orderStatus(Request $request){
        $order=Order::find($request->id);
        $request['order_id']=$request->id;
        $request['user_id']=Auth::user()->id;
        $request['trans_id']=substr(md5(microtime()), 0, 10);

            $request['reason']='Full Payment';
            $request['amount_paid']=$order->amount;
            $request['status'] = 'inprogress';
            $order->update($request->all());
            $log=PaymentLogs::create($request->all());

            $paypal_logs=PayPalLogs::create($request->all());
         $wallet=Wallet::where('user_id',Auth::user()->id)->update(['amount'=>$request->wallet]);

          $user_id=Auth::user()->id;
          $code_detail=ReferalCode::where('code',$order->promo_code)->first();
          $code_check=ReferalCode::where('code',$order->promo_code)->count();
          if($code_check<=0 || $user_id==$code_detail->user_id){
              //Do nothing
          }else{
           $referer_walet=Wallet::where('user_id',$code_detail->user_id)->sum('amount');
           $referer_balance=$referer_walet+10;
              $results = DB::select( DB::raw("INSERT INTO wallets(user_id,amount)VALUES('$code_detail->user_id','$referer_balance') ON DUPLICATE KEY UPDATE amount='$referer_balance'") );
              $request['referer_id']=$code_detail->user_id;
              $request['code']=$order->promo_code;
              $r=ReferingLogs::create($request->all());
          }


        $Notif_Api=env('Notif_Api');
        $admins=Admin::where('role','admin')->get();
        foreach ($admins as $admin){
            $data2=['message'=>'A client with email '.Auth::user()->email.' has PAID an order  '.'. @devmyessay','phone'=>$admin->phone];
            $response2 = Http::withHeaders(['Content-Type'=>'application/json'])->post($Notif_Api.'sms',$data2);
        }
        return ['status'=>true,'message'=>'Payment successfully made.Redirecting...'];
    }


}
