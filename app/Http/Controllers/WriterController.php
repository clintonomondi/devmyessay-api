<?php

namespace App\Http\Controllers;

use App\Models\Files;
use App\Models\Order;
use App\Models\PagesPowerpoint;
use App\Models\Revision;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class WriterController extends Controller
{
    public  function getOrderData(){
        $id=Auth::user()->id;
        $pending=Order::where('taken','No')->where('status','=','inprogress')->count();
        $inprogress=Order::where('status','inprogress')->where('writer_id',Auth::user()->id)->count();
        $revised=Order::where('status','revised')->where('writer_id',Auth::user()->id)->count();
        $completed=Order::where('status','completed')->where('writer_id',Auth::user()->id)->count();
        $onhold=Order::where('status','onhold')->where('writer_id',Auth::user()->id)->count();
        $percent=PagesPowerpoint::where('name','writer')->sum('amount');
        $amount = DB::select( DB::raw("SELECT SUM(amount)AS amount FROM orders WHERE STATUS='completed'  AND writer_id='$id' AND id NOT IN (SELECT order_id FROM writer__billings WHERE user_id='$id')") );
        $wallet=$amount[0]->amount *$percent/100;
        return ['status'=>true,'pending'=>$pending,'inprogress'=>$inprogress,
            'revised'=>$revised,'completed'=>$completed,'onhold'=>$onhold,'wallet'=>$wallet];
    }

    public  function orders(){
        $orders = DB::select( DB::raw("SELECT created_at,submited_at,id,title,pages,deadline,UPPER(code)as code,FORMAT(amount,2)as amount,
(SELECT NAME FROM subjects B WHERE B.id=A.subject_id)subject,
(SELECT VALUE FROM agencies B WHERE B.id=A.agency_id)value,
(SELECT prefix FROM agencies B WHERE B.id=A.agency_id)prefix
 FROM orders A WHERE taken='No' AND status='inprogress' ORDER BY id DESC") );
        $percent=PagesPowerpoint::where('name','writer')->sum('amount');

        return ['orders'=>$orders,'percent'=>$percent];
    }

    public  function bid(Request $request,$id){
        $order=Order::find($id);
        if($order->taken=='Yes'){
            return ['status'=>false,'message'=>'The order has already been taken'];
        }
        $request['taken']='Yes';
        $request['writer_id']=Auth::user()->id;
        $order->update($request->all());
        $Notif_Api=env('Notif_Api');
        $data=['message'=>'You have successfully bid Order ID  '.$order->code, 'email'=>Auth::user()->email, 'subject'=>'ORDER BIDDING'];
        $data2=['message'=>'You have successfully bid Order ID  '.$order->code.'.  @DevMyEssay','phone'=>Auth::user()->phone];
        $response = Http::withHeaders(['Content-Type'=>'application/json'])->post($Notif_Api.'email',$data);
        $response2 = Http::withHeaders(['Content-Type'=>'application/json'])->post($Notif_Api.'sms',$data2);

        return ['status'=>true,'message'=>'Order taken successfully'];
    }


    public  function upload(Request $request,$id){
        $order=Order::find($id);
        $request['order_id']=$id;
        $request['type']='answer';
        $request['user_id']=$order->user_id;
        if(!empty($request->files)) {
            if (($request->has('files'))) {
                $files = $request->file('files');
                foreach ($files as $file) {
                    $fileNameWithExt = $file->getClientOriginalName();
                    $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
                    $extesion = $file->getClientOriginalExtension();
                    $fileNameToStore = $filename . '_' . time() . '.' . $extesion;
                    $path = $file->storeAs('/public/avatars', $fileNameToStore);
                    $request['name'] = $fileNameToStore;
                    $doc = Files::create($request->all());
                }
            }
        }
        $request['status']='completed';
        $order->update($request->all());
        $rev=Revision::where('order_id',$id)->update(['status'=>'Sorted']);

        $client=User::where('id',$order->user_id)->first();
        $Notif_Api=env('Notif_Api');
        $data=['message'=>'This is to Notify that Order Titled '.$order->title.' with ID  '.$order->code.' has been successfully  completed, please login to DevMyEssay to check', 'email'=>$client->email, 'subject'=>'ORDER COMPLETED'];
        $response = Http::withHeaders(['Content-Type'=>'application/json'])->post($Notif_Api.'email',$data);

        return['status'=>true,'message'=>'Files uploaded successfully'];
    }





    public  function inprogress(){
        $orders=\App\Models\Order::orderBy('id','desc')->where('writer_id',Auth::user()->id)->where('status','inprogress')->get();
        $percent=PagesPowerpoint::where('name','writer')->sum('amount');
        return ['orders'=>$orders,'percent'=>$percent];
    }
    public  function onhold(){
        $orders=\App\Models\Order::orderBy('id','desc')->where('writer_id',Auth::user()->id)->where('status','onhold')->get();
        $percent=PagesPowerpoint::where('name','writer')->sum('amount');
        return ['orders'=>$orders,'percent'=>$percent];
    }
    public  function revised(){
        $orders=\App\Models\Order::orderBy('id','desc')->where('writer_id',Auth::user()->id)->where('status','revised')->get();
        $percent=PagesPowerpoint::where('name','writer')->sum('amount');
        return ['orders'=>$orders,'percent'=>$percent];
    }
    public  function completed(){
        $orders=\App\Models\Order::orderBy('id','desc')->where('writer_id',Auth::user()->id)->where('status','completed')->get();
        $percent=PagesPowerpoint::where('name','writer')->sum('amount');
        return ['orders'=>$orders,'percent'=>$percent];
    }
}
