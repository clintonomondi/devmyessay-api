<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Other_services extends Model
{
    use HasFactory;

    protected $fillable=['additional_id','order_id','user_id'];

    public  function order(){
        return $this->belongsTo(Order::class);
    }
    public function additional(){
        return $this->belongsTo(Additonal::class);
    }
}
