<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentLogs extends Model
{
    use HasFactory;

    protected  $fillable=['user_id','order_id','amount_paid','type','reason','trans_id','flag','method'];

    public  function order(){
        return $this->belongsTo(Order::class);
    }
}
