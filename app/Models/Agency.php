<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    use HasFactory;

    protected $fillable=['value','amount','type','prefix','dis'];

    public  function order(){
        return $this->hasMany(Order::class);
    }
}
