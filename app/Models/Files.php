<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Files extends Model
{
    use HasFactory;

    protected  $fillable=['order_id','user_id','name','type'];

    public  function order(){
        return $this->belongsTo(Order::class);
    }
}
