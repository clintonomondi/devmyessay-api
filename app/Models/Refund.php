<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Refund extends Model
{
    use HasFactory;

    protected  $fillable=['user_id','order_id','amount','reason','approved_by','status','comment','updated_by'];

    public  function order(){
        return $this->belongsTo(Order::class);
    }
}
