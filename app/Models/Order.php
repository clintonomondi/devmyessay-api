<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected  $fillable=['typeofservice_id','typeofpaper_id','subject_id','instruction','title','agency_id','academic_id','pages',
        'writerlevel_id','prefered_id','discount_code','user_id','status','amount','code'   ,'powerpointchecked',
        'powerpointslides','style','pagespacing','sources','writer',
        'customer_service','deadline','submited_at','logins','writer_id','taken','promo_code'];

    public  function user(){
        return $this->belongsTo(User::class);
    }

    public  function agency(){
        return $this->belongsTo(Agency::class);
    }

    public  function typeofservice(){
        return $this->belongsTo(Typeofservice::class);
    }

    public  function subject(){
        return $this->belongsTo(Subject::class);
    }

    public  function typeofpaper(){
        return $this->belongsTo(Typeofpaper::class);
    }

    public  function academic(){
        return $this->belongsTo(Academic::class);
    }
    public  function payment_logs(){
        return $this->hasMany(PaymentLogs::class);
    }
    public  function other_services(){
        return $this->hasMany(Other_services::class);
    }
    public  function refund(){
        return $this->hasMany(Refund::class);
    }
    public  function files(){
        return $this->hasMany(Files::class);
    }
    public  function paypallogs(){
        return $this->hasMany(PayPalLogs::class);
    }
}
