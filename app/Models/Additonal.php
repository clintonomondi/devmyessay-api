<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Additonal extends Model
{
    use HasFactory;

    protected $fillable=['name','amount','incremental'];

public  function other_services(){
    return $this->hasMany(Other_services::class);
}
}
