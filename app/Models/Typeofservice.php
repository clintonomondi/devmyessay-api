<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Typeofservice extends Model
{
    use HasFactory;

    protected $fillable=['name','amount'];

    public  function order(){
        return $this->hasMany(Order::class);
    }
}
