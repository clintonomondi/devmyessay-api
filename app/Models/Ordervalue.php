<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ordervalue extends Model
{
    use HasFactory;

    protected  $fillable =['order_id','service_value','paper_value','agency_value','academics_value','pages_value','writerlevel_value','powerpointcheckvalue_value','writer_value','additonal_value','customer_service_value','subject_value'];

}
