<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdervaluesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordervalues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_id');
            $table->string('service_value')->default('0');
            $table->string('paper_value')->default('0');
            $table->string('agency_value')->default('0');
            $table->string('academics_value')->default('0');
            $table->string('pages_value')->default('0');
            $table->string('writerlevel_value')->default('0');
            $table->string('powerpointcheckvalue_value')->default('0');
            $table->string('writer_value')->default('0');
            $table->string('additonal_value')->default('0');
            $table->string('customer_service_value')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordervalues');
    }
}
