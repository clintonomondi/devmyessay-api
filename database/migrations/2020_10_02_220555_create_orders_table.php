<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('typeofservice_id');
            $table->string('typeofpaper_id');
            $table->string('subject_id');
            $table->text('instruction')->nullable();
            $table->string('title');
            $table->string('agency_id');
            $table->string('academic_id');
            $table->string('pages');
            $table->string('writerlevel_id')->nullable();
            $table->string('prefered_id')->nullable();
            $table->string('discount_code')->nullable();
            $table->string('user_id');
            $table->string('status')->default('pending');
            $table->string('amount');
            $table->string('code');

            $table->string('powerpointchecked')->nullable();
            $table->string('powerpointslides')->nullable();
            $table->string('style')->nullable();
            $table->string('pagespacing')->nullable();
            $table->string('sources')->nullable();
            $table->string('writer')->nullable();
            $table->string('customer_service')->nullable();
            $table->dateTime('deadline')->nullable();
            $table->string('submited_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
